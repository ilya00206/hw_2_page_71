#!/usr/bin/env python3
"""Task 3 from page 71"""
import typing


class Cube:
    """Cube class"""
    def __init__(self, a: typing.Union[int, float]):
        self.a = a
        self.volume = a ** 3

    def __eq__(self, other):
        """Compares if objects are equal"""
        return self.volume == other.volume

    def __lt__(self, other):
        """Compares if object is less than another"""
        return self.volume < other.volume

    def __gt__(self, other):
        """Compares if object is greater than another"""
        return self.volume > other.volume

    def __le__(self, other):
        """Compares if object is less or equals another"""
        return self.volume <= other.volume

    def __ge__(self, other):
        """Compares if object is greater or equals another"""
        return self.volume >= other.volume

    def __ne__(self, other):
        """Compares if objects are not equal"""
        return self.volume != other.volume


cube1 = Cube(10)
cube2 = Cube(15)
print("cube1 == cube2? " + str(cube1 == cube2))
print("cube1 != cube2? " + str(cube1 != cube2))
print("cube1 < cube2? " + str(cube1 < cube2))
print("cube1 <= cube2? " + str(cube1 <= cube2))
print("cube1 > cube2? " + str(cube1 > cube2))
print("cube1 >= cube2? " + str(cube1 >= cube2))
