#!/usr/bin/env python3
"""Task 2 from page 71"""
import typing


class Rectangle:
    """Rectangle class"""
    def __init__(self, a: typing.Union[int, float], b: typing.Union[int, float]):
        self.a = a
        self.b = b
        self.square = a * b

    def __eq__(self, other):
        """Compares if objects are equal"""
        return self.square == other.square

    def __lt__(self, other):
        """Compares if object is less than another"""
        return self.square < other.square

    def __gt__(self, other):
        """Compares if object is greater than another"""
        return self.square > other.square

    def __le__(self, other):
        """Compares if object is less or equals another"""
        return self.square <= other.square

    def __ge__(self, other):
        """Compares if object is greater or equals another"""
        return self.square >= other.square

    def __ne__(self, other):
        """Compares if objects are not equal"""
        return self.square != other.square


rect1 = Rectangle(5, 6)
rect2 = Rectangle(6, 5)
print("rect1 == rect2? " + str(rect1 == rect2))
print("rect1 != rect2? " + str(rect1 != rect2))
print("rect1 < rect2? " + str(rect1 < rect2))
print("rect1 <= rect2? " + str(rect1 <= rect2))
print("rect1 > rect2? " + str(rect1 > rect2))
print("rect1 >= rect2? " + str(rect1 >= rect2))
