#!/usr/bin/env python3
"""Task 4 from page 71"""


class Vehicle:
    """Vehicle abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        self.speed: int = speed

    def __eq__(self, other):
        """Compares if objects are equal"""
        return self.speed == other.speed

    def __lt__(self, other):
        """Compares if object is less than another"""
        return self.speed < other.speed

    def __gt__(self, other):
        """Compares if object is greater than another"""
        return self.speed > other.speed

    def __le__(self, other):
        """Compares if object is less or equals another"""
        return self.speed <= other.speed

    def __ge__(self, other):
        """Compares if object is greater or equals another"""
        return self.speed >= other.speed

    def __ne__(self, other):
        """Compares if objects are not equal"""
        return self.speed != other.speed
    pass


class MotorVehicle(Vehicle):
    """Motor vehicle abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


class Aircraft(Vehicle):
    """Aircraft abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


class Car(MotorVehicle):
    """Car abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


class Truck(MotorVehicle):
    """Truck abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


class Airplane(Aircraft):
    """Airplane abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


class Helicopter(Aircraft):
    """Helicopter abstraction class."""
    def __init__(self, speed: int):
        """Ctor that takes 1 parameter"""
        super().__init__(speed)
    pass


car: Car = Car(50)
car2: Car = Car(100)
plane: Airplane = Airplane(300)
print("Car 2 is faster than car? " + str(car2 > car))
print("Car 2 is faster than plane? " + str(car2 > plane))

