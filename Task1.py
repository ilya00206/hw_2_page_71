#!/usr/bin/env python3
"""Task 1 from page 71"""


def get_name():
    your_name: str = input("What is your name? ")
    if your_name == "Ilya":
        print("Hello, my creator")
    else:
        print("Hello, user " + your_name)


get_name()
